<?php

class TestHttpClientFactory extends \PHPUnit\Framework\TestCase
{

    /**
     * Test createClient method.
     */
    public function testCreateClient()
    {
        $options = Mockery::mock(\WPDesk\HttpClient\HttpClientOptions::class);
        $options->shouldReceive('getHttpClientClass')
            ->withAnyArgs()
            ->andReturn(\WPDesk\HttpClient\Curl\CurlClient::class);
        $factory = new \WPDesk\HttpClient\HttpClientFactory();
        $client = $factory->createClient($options);
        $this->assertInstanceOf(\WPDesk\HttpClient\Curl\CurlClient::class, $client);
    }


}